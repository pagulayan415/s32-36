// Business logic
    // including model methods

const { createAccessToken } = require('../auth');
const User = require('./../models/userSchema');
const auth = require('./../auth.js');
const bcrypt = require('bcrypt');
const { findByIdAndDelete, findOneAndDelete } = require('./../models/userSchema');
const Course = require('../models/Course');

// Check email routes
module.exports.checkEmail = (email) =>{
    return User.findOne({email: email})
    .then((result, error) =>{
        if(result != null){
            return false;
        }else{
            if(result == null){
                return true;
            }else{
                return error;
            }
        }
    })
}

module.exports.register = (reqbody) =>{
    // save/create a new user document
        // using .save() method to the database
    // console.log(reqbody) //object user document

    // how to use object destructuring
        // why? to make distict variables for each property w/o using dot notation
        // const {properties} = <object reference> 
    const {firstName, lastName, email, password, mobileNo, age} = reqbody
    // console.log(firstName)
    const newUser = new User({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: bcrypt.hashSync(password, 10),
        mobileNo: mobileNo,
        age: age
    })
    // save the newUser object to the database
    return newUser.save().then((result, error) => {
        //console.log(result) //document
        if(result){
            result.password = "*****"
            return result;
        }else{
            return error;
        }
    })
}

// get all users records
module.exports.getAllUsers = () => {
    return User.find({})
    .then((result)=>{
        return result;
    })
}

// Log-in user
module.exports.login = (reqBody) =>{
    const {email, password} = reqBody
    return User.findOne({email: email})
    .then((result, error) =>{
        if(result == null){
            console.log('email null');
            return false;
        }else{
            let isPwCorrect = bcrypt.compareSync(password, result.password) //boolean bec it compares two arguments
            if(isPwCorrect == true){
                // return json web token
                // invoke the function which creates the token upon loggin in
                    // requirements in creating a token
                        // if password matches from existing pw from db
                        // return {access: auth.createAccessToken(result)}
                return {access: auth.createAccessToken(result)}
            }else{
                return false;
            }
        }
    })
}

module.exports.getUserProfile = (reqBody) =>{
    return User.findById({_id: reqBody.id})
    .then(result => {return result;
	})
}

module.exports.editProfile = (userId, reqBody) =>{
    // console.log(userId)
    // console.log(reqBody)
    return User.findById(userId, reqBody, {returnDocument:'after'})
    .then( result =>{
        result.password = "****"
        return result})
}

module.exports.editUser = (userId, reqBody) =>{
    console.log(userId)
    console.log(reqBody)
    const {firstName, lastName, email, password, mobileNo, age} = reqBody

    const updatedUser = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNo,
        age: age,
        password: bcrypt.hashSync(password, 10)
    }

    return User.findByIdAndUpdate(userId, updatedUser, {returnDocument:'after'}.hashSync)
    .then(result =>{ 
        result.password = "******"
        return result})

}

module.exports.editDetails = (userEmail, reqBody) =>{
    // console.log(email)
    const {firstName, lastName, email, password, mobileNo, age} = reqBody

    const updatedDetails = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNo,
        age: age,
        password: bcrypt.hashSync(password, 10)
    }

        return User.findOneAndUpdate(userEmail, updatedDetails, {returnDocument:'after'}.hashSync)
        .then(result =>{ 
            result.password = "******"
            return result})
}

module.exports.delete = (userId) =>{
    return User.findByIdAndDelete(userId)
    .then((removedUser, err)=>{
        if(err){
            return false;
        }else{
            // return removedUser;
            return true;
        }
    })
}

module.exports.deleteUser = (userEmail) =>{
    console.log(userEmail)
    return User.findOneAndDelete(userEmail)
    .then((deletedUser, err)=>{
        if(err){
            return false;
        }else{
            // return deletedUser;
            return true;
        }
    })
}

module.exports.enroll = async (data) =>{
    const {userId, courseId} = data;
    
    const userEnroll = await User.findById(userId)
    .then((result, err)=>{
        if(err){
            return err;
        }else{
            result.enrollments.push({
                courseId: courseId
            })
            return result.save()
            .then(result => true)
        }
    })
    // look for matching document of a user
    const courseEnroll = await Course.findById(courseId).then((result, err)=>{
        if(err){
            return err;
        }else{
            result.enrollees.push({
                userId: userId
            })
            return result.save()
            .then(result=> {return true})
        }
    })
    // to return only one value for the function enroll
    if(userEnroll && courseEnroll){
        return true;
    }else{
        return false;
    }
}