const { createAccessToken } = require('../auth');
const Course = require('./../models/Course');
const auth = require('./../auth.js');
const bcrypt = require('bcrypt');
const { findByIdAndDelete, findOneAndDelete } = require('./../models/Course');


// get all courses
module.exports.getAllCourses = () =>{
    return Course.find({})
    .then((result)=>{
        // console.log('courses')
        return result;
    })
}

// Add course
module.exports.createCourse = (reqBody) => {
    const {courseName, description, price} = reqBody

    const newCourse = new Course({
        courseName: courseName,
        description: description,
        price: price
    })

    return newCourse.save()
    .then((result, error) =>{
        if(result){
            return result;
        }else{
            return error;
        }
    })
}

// Retrieve isActive courses
module.exports.getActiveCourses = ()=>{
    return Course.find({})
    .then((result, error)=>{
        if(result == true){
            return result;
        }
    })
}