// use diirective keyword require to make a module useful in the current file/module
const express = require('express');

// express functon is our server stored in a constant variable app
const app = express();
const cors = require('cors');

// require mongoose module to be used in our entry point file index.js
const mongoose = require('mongoose')

const port = 3001;

// const User = require('./models/userSchema');

// MIDDLEWARES
// Express.json is an express framework to parse incoming json payloads
app.use(express.json());
app.use(express.urlencoded({extended:true}))


// Connecting userRoutes module to index.js entry point
const userRoutes = require('./routes/userRoutes')
const courseRoutes = require('./routes/courseRoutes')

// connect to mongoDB database
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.u57jm.mongodb.net/courseBooking?retryWrites=true&w=majority',
{
    useNewUrlParser: true,
    useUnifiedTopology: true
})

// Notification if connected to the database
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log('Connected to Database'));

// routes
    // http://localhost:3001/users
    // HTTP METHODS

    // Request
        // params
        // body
        // url
        // HTTP methods
        // GET
        // POST
        // PUT
        // PATCH
        // DELETE
        // Headers
            // Authorizations
    // Response
        // send

// app.get('/users', (req, res) => {
//     // console.log('Hello')
//     res.send(req.body)
// })

// app.post('/users', (req, res) =>{
//     // res.send(req.body)
//     let name = req.body.name;
//     res.send(`Hi ${name}`)
// })


// middleware entry point url (root url before any endpoints)
app.use('/api/users', userRoutes)
app.use('/api/courses', courseRoutes)

app.listen(port, ()=>{
    console.log(`Server is running at port ${port}`)
})