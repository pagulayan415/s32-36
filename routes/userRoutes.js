// Create routes
const express = require('express')

// Router() handles the requests
const router = express.Router();

// User model
// const User = require('./../models/userSchema')

// User Controller
const userController = require('./../controllers/userControllers')

const auth = require('./../auth');


// syntax: router.HTTPmethod('uri', <request listener>)
// router.get('/', (req, res) =>{
//     // console.log('Hello from userRoutes')
//     res.send('Hello from userRoutes')
// })

// router.post('/email-exists', (req, res) => {
//     let email = req.body.email
//     // find the matchin document in the database using email
//         // by using Model methods
//     User.findOne({email: email})
//     .then((result, error) =>{
//         if(result != null){
//             res.send(false)
//         }else{
//             if(result == null){
//                 res.send(true)
//             }else{
//                 res.send(error)
//             }
//         }
//     })
//     // send back the response to the client
// })

// Check email
router.post('/email-exists', (req, res) =>{
    // invoke the function here
    userController.checkEmail(req.body.email)
    .then(result => res.send(result))
});

// register route
router.post('/register', (req, res) =>{
    // expecting data/object coming from the request body
    userController.register(req.body)
    .then(result => res.send(result))
})

// mini activity
// create a route to get all users
router.get('/', (req, res) =>{
    userController.getAllUsers()
    .then(results => res.send(results))
})

// user log-in
router.post('/login', (req, res) => {
    userController.login(req.body)
    .then(results => res.send(results))
})

// retrieve the user details
router.get('/details', auth.verify, (req, res) =>{
    // console.log(auth.decode(req.headers.authorization))
    // auth.decode(req.headers.authorization)
    // userController.getUserProfile()
    // .then(result => res.send(result))
    let userData = auth.decode(req.headers.authorization)
    userController.getUserProfile(userData)
    .then(result => res.send(result))
})

// edit user information
router.put('/:userId/edit', (req, res)=>{
    // console.log(req.body)
    const userId = req.params.userId
    userController.editProfile(userId, req.body)
    .then(result => res.send(result))
})

router.put('/edit', auth.verify, (req, res)=>{
    // console.log(req.headers.authorization)
    // console.log(auth.decode(req.headers.authorization).id)
    let userId = auth.decode(req.headers.authorization);

    userController.editUser(userId.id, req.body)
    .then(result => res.send(result))
})

// mini activity
    // create a route to update user detals with the following:
    // endpoint = "/edit-profile"
    // method: put
    // use email as filter to findOne() method
router.put('/edit-profile', (req, res)=>{
    let email = req.body.email;
    // console.log(email)
    userController.editDetails(email, req.body)
    .then(result => res.send(result))
})

// mini activity
    // create a route to delete a specific user with the followng:
        // endpoint = "/:userId/delete"
        // function name: delete()
        // method: delete
        // use id as filter to fineOneAndDelte method
router.delete('/:userId/delete', (req, res)=>{
    let userId = req.params.userId
    // console.log(userId)
    userController.delete(userId)
    .then((result)=> res.send(result))
})


//mini activity
	// create a route to delete a specific user with the following:
		// endpoint = "/delete"
		// function name: deleteUser()
		// method: delete
		// use email as filter to findOneAndDelete method
		// return true if document was successfully deleted
router.delete('/delete', (req, res) =>{
    email = req.body.email;
    // console.log(email)
    userController.deleteUser(email)
    .then((results) => res.send(results))
})

// enrollments
router.post('/enroll', auth.verify, (req, res)=>{
    let data = {
        // user id of the logged in user
        userId: auth.decode(req.headers.authorization).id,
        // course id of the course you're enrolling in, to be supplied by the user
        courseId: req.body.courseId
    }
    userController.enroll(data)
    .then(result => res.send(result))
})

module.exports = router;



